from pathlib import Path

instructions = Path('day1_input.txt').read_text().strip()

ups = instructions.count('(')

print(ups*2 - len(instructions))

floor = 0
position = 0

while floor >= 0:
    if instructions[position] == '(':
        floor += 1
    else:
        floor -= 1
    position += 1

print(position)
