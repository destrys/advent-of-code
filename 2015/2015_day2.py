area = 0

with open("day2_input.txt") as file:
    for line in file:
        s = sorted([int(side) for side in line.strip().split('x')])
        area += ((3 * s[0] * s[1]) + (2 * s[1] * s[2]) + (2 * s[2] * s[0]))

print("Part 1: ", area)

#-------

length = 0

with open("day2_input.txt") as file:
    for line in file:
        s = sorted([int(side) for side in line.strip().split('x')])
        length += 2 * (s[0] + s[1]) + s[0] * s[1] * s[2]

print("Part 2: ", length)
