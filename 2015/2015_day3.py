import operator

from pathlib import Path

instructions = Path('day3_input.txt').read_text().strip()

visited_houses = {(0,0)}

location = (0, 0)

moves = {">": (1, 0),
         "<": (-1, 0),
         "^": (0, 1),
         "v": (0, -1)}

for char in instructions:
    location = tuple(map(operator.add, location, moves[char]))
    visited_houses.add(location)

print("Part 1: ", len(visited_houses))

#-------

houses = [{(0,0)},{(0,0)}]

locations = [(0, 0),(0, 0)]

moves = {">": (1, 0),
         "<": (-1, 0),
         "^": (0, 1),
         "v": (0, -1)}

for char in instructions:
    locations[0] = tuple(map(operator.add, locations[0], moves[char]))
    houses[0].add(locations[0])
    houses.reverse()
    locations.reverse()

visited_houses = houses[0].union(houses[1])

#print(visited_houses)
print("Part 2: ", len(visited_houses))
