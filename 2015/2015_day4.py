from hashlib import md5

input = 'ckczppom'

index = 0

while True:
    output = input + str(index)
    hex_digest = md5(output.encode()).hexdigest()
    if hex_digest[:5] == "00000":
        break
    index += 1

print("Part 1: ", index, hex_digest)

#------

while True:
    output = input + str(index)
    hex_digest = md5(output.encode()).hexdigest()
    if hex_digest[:6] == "000000":
        break
    index += 1

print("Part 2: ", index, hex_digest)
