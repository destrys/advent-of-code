
nice_strings = 0

def has_three_vowels(word):
    num_vowels = 0
    vowels = set('aeiou')
    for char in word:
        if char in vowels:
            num_vowels += 1
        if num_vowels == 3:
            return True
    return False

def has_duplicate_letters(word):
    prev_letter = ''
    for char in word:
        if char == prev_letter:
            return True
        prev_letter = char
    return False

def contains_bad_pair(word):
    naughty_pairs = ['ab', 'cd', 'pq', 'xy']
    for pair in naughty_pairs:
        if pair in word:
            return True
    return False
        

def is_nice(word):
    if (has_three_vowels(word)
        and has_duplicate_letters(word)
        and not contains_bad_pair(word)):
        return True
    else:
        return False

with open("day5_input.txt") as file:
    for line in file:
        nice_strings += is_nice(line.strip())

print("Part 1: ", nice_strings)


def has_two_pair(word):
    pairs = dict()
    prev_char = ''
    for index, char in enumerate(word):
        pair = f'{prev_char}{char}'
        if pair in pairs:
            if index - pairs[pair] > 1:
                return True
        else:
            pairs[pair] = index
        prev_char = char
    return False

def has_separated_pair(word):
    prev_2 = ''
    prev = ''
    for char in word:
        if char == prev_2:
            return True
        prev_2 = prev
        prev = char
    return False

def is_real_nice(word):
    if (has_two_pair(word) and has_separated_pair(word)):
        return True
    else:
        return False

very_nice_strings = 0
with open("day5_input.txt") as file:
    for line in file:
        very_nice_strings += is_real_nice(line.strip())

print("Part 2: ", very_nice_strings)
