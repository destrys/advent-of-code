# set up list:

rows = [bytearray([0]*1000) for _ in range(1000)]

def modify_lights(row1, row2, col1, col2, method):
    for row in rows[row1:row2+1]:
        if method == 'turn on':
            row[col1:col2+1] = bytearray([1]*(col2-col1+1))
        if method == 'turn off':
            row[col1:col2+1] = bytearray([0]*(col2-col1+1))
        if method == 'toggle':
            flip = int.from_bytes(bytearray([1]*(col2-col1+1)), 'bs
            interest = row[col1:col2+1]
            
