# both driven by a precise clock circuit. The clock circuit ticks at a constant rate; each tick is called a cycle.

# Start by figuring out the signal being sent by the CPU. The CPU has a single register, X, which starts with the value 1. It supports only two instructions:

#  addx V takes two cycles to complete. After two cycles, the X register is increased by the value V. (V can be negative.)
#  noop takes one cycle to complete. It has no other effect.

# signal strength (the cycle number multiplied by the value of the X register)

# Find the signal strength during the 20th, 60th, 100th, 140th, 180th, and 220th cycles. What is the sum of these six signal strengths?

class Register:
    def __init__(self, init_val):
        self.x  = init_val
        self.history = []

    def noop(self):
        self.history.append(self.x)

    def addx(self, v):
        self.history.append(self.x)
        self.history.append(self.x)        
        self.x += v


register = Register(1)

with open("day10_input.txt") as file:
    for line in file:
        if "noop" in line:
            register.noop()
        else:
            register.addx(int(line.strip().split()[1]))

cycles = [20, 60, 100, 140, 180, 220]

part1 = sum([cycle * register.history[cycle-1] for cycle in cycles])

print("Part 1: ", part1)


image = ['.'] * len(register.history)

for i in range(len(register.history)):
    x_pos = i % 40
    overlap = abs(x_pos-register.history[i]) < 2
    print(x_pos, register.history[i])
    if overlap:
        image[i] = '#'

for i in range(6):
    print(''.join(image[40*i:(40*(i+1))]))
    
