import math

def create_operation(equation):
    def operation(old):
        return eval(equation)
    return operation
    
            
class Monkey:
    def __init__(self, items, operation, divisible, t_monkey, f_monkey, monkey_list):
        self.items = items
        self.operation = operation
        self.divisible_check = divisible
        self.true_monkey_index = t_monkey
        self.false_monkey_index = f_monkey
        self.monkey_list = monkey_list
        self.inspected_counter = 0

    def calculate_worry_level(self, item_val):
        #return math.floor(self.operation(item_val) / 3)
        return self.operation(item_val)

    def inspect_and_throw(self):
        for item in self.items:
            self.inspected_counter += 1
            worry_level = self.calculate_worry_level(item)
            if not(worry_level % self.divisible_check):
                self.monkey_list[self.true_monkey_index].items.append(worry_level)
            else:
                self.monkey_list[self.false_monkey_index].items.append(worry_level)
        self.items = []

# make monkey array

monkey_list = []

with open("day11_input_example.txt") as file:
    for line in file:
        match line.strip().split():
            case ['Starting', 'items:', *items]:
                items = [int(i.strip(',')) for i in items]
            case ['Operation:', 'new', '=', *equation_parts]:
                equation = ' '.join(equation_parts)
                operation = create_operation(equation)
            case ['Test:', 'divisible', 'by', divisible]:
                divisible = int(divisible)
            case ['If', 'true:', 'throw', 'to', 'monkey', t_monkey]:
                t_monkey = int(t_monkey)
            case ['If', 'false:', 'throw', 'to', 'monkey', f_monkey]:
                f_monkey = int(f_monkey)
                monkey_list.append(
                    Monkey(items, operation, divisible, t_monkey,
                           f_monkey, monkey_list)
                )
            case _:
                pass


            
            
for i in range(10000):
    for monkey in monkey_list:
        print(i)
        monkey.inspect_and_throw()
    if i in range(1000,10000,1000):
        print([m.inspected_counter for m in monkey_list])

inspections = list(sorted([m.inspected_counter for m in monkey_list]))
print(inspections[-1] * inspections[-2])
