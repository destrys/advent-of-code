class Item:
    def __init__(self, value, factors):
        self.factors = factors
        self.remainders = [0]*len(factors)
        for i, f in enumerate(factors):
            self.remainders[i] = value % f

    def __add__(self, y):
        for i, f in enumerate(factors):
            self.remainders[i] = (self.remainders[i] + y) % f
        return self

    def __mul__(self, other):
        for i, f in enumerate(factors):
            if isinstance(other, int):
                self.remainders[i] = (self.remainders[i] * other) % f
            if isinstance(other, Item):
                self.remainders[i] = (self.remainders[i] * other.remainders[i]) % f
        return self

    def __mod__(self, y):
        index = self.factors.index(y)
        return self.remainders[index]


class Monkey:
    def __init__(self, items, operation, divisible, t_monkey, f_monkey, monkey_list, factors):
        self.items = [Item(i, factors) for i in items]
        self.operation = operation
        self.divisible_check = divisible
        self.true_monkey_index = t_monkey
        self.false_monkey_index = f_monkey
        self.monkey_list = monkey_list
        self.inspected_counter = 0

    def calculate_worry_level(self, item_val):
        return self.operation(item_val)

    def inspect_and_throw(self):
        for item in self.items:
            self.inspected_counter += 1
            worry_level = self.calculate_worry_level(item)
            if not(worry_level % self.divisible_check):
                self.monkey_list[self.true_monkey_index].items.append(worry_level)
            else:
                self.monkey_list[self.false_monkey_index].items.append(worry_level)
        self.items = []

def create_operation(equation):
    def operation(old):
        return eval(equation)
    return operation


monkey_list = []
factors = []
data_file = "day11_input.txt"

## Find Factors

with open(data_file) as file:
    for line in file:
        if 'divisible' in line:
            factors.append(int(line.strip().split()[-1]))

## Make Monkey List
            
with open(data_file) as file:
    for line in file:
        match line.strip().split():
            case ['Starting', 'items:', *items]:
                items = [int(i.strip(',')) for i in items]
            case ['Operation:', 'new', '=', *equation_parts]:
                equation = ' '.join(equation_parts)
                operation = create_operation(equation)
            case ['Test:', 'divisible', 'by', divisible]:
                divisible = int(divisible)
            case ['If', 'true:', 'throw', 'to', 'monkey', t_monkey]:
                t_monkey = int(t_monkey)
            case ['If', 'false:', 'throw', 'to', 'monkey', f_monkey]:
                f_monkey = int(f_monkey)
                monkey_list.append(
                    Monkey(items, operation, divisible, t_monkey,
                           f_monkey, monkey_list, factors)
                )
            case _:
                pass

## Inspect and Throw
            
for i in range(10000):
    for monkey in monkey_list:
        monkey.inspect_and_throw()

inspections = list(sorted([m.inspected_counter for m in monkey_list]))
print('MONKEY BUSINESS: ', inspections[-1] * inspections[-2])
