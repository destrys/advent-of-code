from collections import deque

def check_neighbors(x, y, steps, grid):
    current_position = map_grid[y][x]
    next_steps = []
    if current_position == 'X': # already been here
        return False
    if (x - 1) >= 0:
        if can_visit(current_position, map_grid[y][x-1], steps):
            next_steps.append([x - 1 , y, steps+1])
    if (x + 1) < len(map_grid[0]):
        if can_visit(current_position, map_grid[y][x+1], steps):
            next_steps.append([x + 1 , y, steps+1])
    if (y - 1) >= 0:
        if can_visit(current_position, map_grid[y-1][x], steps):
            next_steps.append([x, y - 1, steps+1])
    if (y + 1) < len(map_grid):
        if can_visit(current_position, map_grid[y+1][x], steps):
            next_steps.append([x, y + 1, steps+1])
    return next_steps

def can_visit(current, destination, steps):
    if destination == 'E':
        destination = 'z'
    if current == 'S':
        current = 'a'
    height_difference = ord(destination) - ord(current)
    if height_difference < 2:
        return True
    return False

# build a grid

map_grid = []
to_check = deque()

with open("day12_input.txt") as file:
    for line in file:
        map_grid.append(list(line.strip()))
        
for i, row in enumerate(map_grid):
    if 'S' in row:
        s_y = i
        s_x = row.index('S')

to_check.append([s_x, s_y, 0])

while len(to_check):
    x, y, steps = to_check.popleft()
    if map_grid[y][x] == 'E':
        print("Part 1: ", steps)
        break
    if adjacent_steps := check_neighbors(x, y, steps, map_grid):
        to_check.extend(adjacent_steps)
    map_grid[y][x] = 'X'
    
grid = [''.join(row) for row in map_grid]
for row in grid:
    print(row)
