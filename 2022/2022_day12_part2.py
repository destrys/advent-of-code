from collections import deque
from copy import deepcopy

def check_neighbors(grid, x, y, steps):
    current_position = grid[y][x]
    next_steps = []
    if current_position == 'X': # already been here
        return False
    if (x - 1) >= 0:
        if can_visit(current_position, grid[y][x-1], steps):
            next_steps.append([x - 1 , y, steps+1])
    if (x + 1) < len(grid[0]):
        if can_visit(current_position, grid[y][x+1], steps):
            next_steps.append([x + 1 , y, steps+1])
    if (y - 1) >= 0:
        if can_visit(current_position, grid[y-1][x], steps):
            next_steps.append([x, y - 1, steps+1])
    if (y + 1) < len(grid):
        if can_visit(current_position, grid[y+1][x], steps):
            next_steps.append([x, y + 1, steps+1])
    return next_steps

def can_visit(current, destination, steps):
    if destination == 'E':
        destination = 'z'
    if current == 'S':
        current = 'a'
    height_difference = ord(destination) - ord(current)
    if height_difference < 2:
        return True
    return False

# build a grid

with open("day12_input.txt") as file:
    map_grid = [list(line.strip()) for line in file]

map_copy = deepcopy(map_grid)

# find start

for i, row in enumerate(map_grid):
    if 'S' in row:
        s_y = i
        s_x = row.index('S')

to_check = deque([[s_x, s_y, 0]])

# do the stuff

def find_shortest_path(to_check, grid):
    while len(to_check):
        x, y, steps = to_check.popleft()
        if grid[y][x] == 'E':
            return steps
        if adjacent_steps := check_neighbors(grid, x, y, steps):
            to_check.extend(adjacent_steps)
        grid[y][x] = 'X'
    return 9999999
    
print("Part 1: ", find_shortest_path(to_check, map_copy))


# -----

a_locations = []

for i, row in enumerate(map_grid):
    if 'a' in row:
        for j, letter in enumerate(row):
            if letter == 'a':
                a_locations.append(deque([[j, i, 0]]))

print(len(a_locations))
counter = 0
route_lengths = []
for start in a_locations:
    print(counter)
    counter += 1
    map_copy = deepcopy(map_grid)
    route_lengths.append(find_shortest_path(start, map_copy))

print("Part 2: ", sorted(route_lengths)[0])

