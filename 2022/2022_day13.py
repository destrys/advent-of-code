# You need to identify how many pairs of packets are in the right order.

from ast import literal_eval
from functools import cmp_to_key

def check_order(left, right):
    match [left, right]:
        case [int(), list()]:
            order = check_order([left], right)
        case [list(), int()]:
            order = check_order(left, [right]) 
        case [int(), int()]:
            order = left <= right
        case [list(), list()]:
            for l, r in zip(left, right):
                if l != r:
                    return check_order(l,r)
            order = len(left) <= len(right) 
    return order


with open("day13_input.txt") as file:
    correct_orders = []
    while line := file.readline():
        left_list = literal_eval(line)
        right_list = literal_eval(file.readline())
        correct_orders.append(check_order(left_list, right_list))
        blank = file.readline()

indices = [i+1 for i, order in enumerate(correct_orders) if order]

print('Part 1:', sum(indices))


def check_order_2(left, right):
    match [left, right]:
        case [int(), list()]:
            order = check_order_2([left], right)
        case [list(), int()]:
            order = check_order_2(left, [right]) 
        case [int(), int()]:
            order = left > right
        case [list(), list()]:
            for l, r in zip(left, right):
                if l != r:
                    return check_order_2(l,r)
            order = len(left) > len(right)
    if order == True or order == 1:
        return 1
    else:
        return -1


with open("day13_input.txt") as file:
    all_packets = [literal_eval(line.strip()) for line in file if line.strip()]
    all_packets.append([[2]])
    all_packets.append([[6]])

sorted_packets = list(sorted(all_packets, key=cmp_to_key(check_order_2)))

divider1_index = sorted_packets.index([[2]]) + 1
divider2_index = sorted_packets.index([[6]]) + 1

print("Part 2:", divider1_index * divider2_index)
