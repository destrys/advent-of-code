from copy import copy

def print_grid(grid):
    for row in grid:
        print(''.join(row))


## build grid

rocks = []

with open("day14_input.txt") as file:
    for line in file:
        coords = line.strip().split('->')
        rocks.append([(int(coord.split(',')[0]),
                       int(coord.split(',')[1])) for coord in coords])


y_vals = [coord[1] for rock in rocks for coord in rock]
y_max = max(y_vals) + 2
x_min = 500 - y_max - 5 
x_max = 500 + y_max + 5

rocks.append([[x_min, y_max], [x_max, y_max]])

grid = [copy([True]*(x_max - x_min + 1)) for _ in range(y_max+1)]
    
for rock in rocks:
    start_vertex = rock.pop()
    while len(rock):
        end_vertex = rock.pop()
        if start_vertex[0] == end_vertex[0]:
            y_coords = sorted([start_vertex[1], end_vertex[1]])
            for y_index in range(y_coords[0], y_coords[1]+1):
                grid[y_index][start_vertex[0]-x_min] = False
        if start_vertex[1] == end_vertex[1]:
            x_coords = sorted([start_vertex[0]-x_min, end_vertex[0]-x_min])
            for x_index in range(x_coords[0], x_coords[1]+1):
                grid[start_vertex[1]][x_index] = False
        start_vertex = end_vertex

######

def next_location(start, grid):
    if not grid[start[1]][start[0]]:
        return None
    if grid[start[1] + 1][start[0]]:
        return (start[0], start[1] + 1)
    if grid[start[1] + 1][start[0] - 1]:
        return (start[0] - 1, start[1] + 1)
    if grid[start[1] + 1][start[0] + 1]:
        return (start[0] + 1, start[1] + 1)
    return start

#### Drop the sand

def drop_sand(grid):
    position = (500-x_min, 0)
    prev = (0,0)
    while position != prev:
        prev = position
        position = next_location(position, grid)
        if position is None:
            return False, grid
    grid[position[1]][position[0]] = False
    return True, grid

counter = 0
test = True
while test:
    counter += 1
    test, grid = drop_sand(grid)
    
print("Part 2:", counter - 1)
#print_grid(grid)

