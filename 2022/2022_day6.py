from pathlib import Path
from collections import deque

last_four = deque()

signals = Path('day6_input.txt').read_text().strip()

position = 1

for char in signals:
    last_four.append(char)
    if len(last_four) > 4:
        last_four.popleft()
    if len(set(last_four)) == 4:
        print(set(last_four))
        print("FOUND", position)
        break
    position += 1
    print(last_four)

