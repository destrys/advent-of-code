from functools import reduce
import operator


# build a filesystem

def cd(filesystem, pwd, path):
    if path == '/':
        return filesystem, ''
    if path == '..':
        dir_list = pwd.split('/')[:-1]
        if not dir_list:
            return filesystem, ''        
        pwd = '/'.join(dir_list)
    else:
        if pwd:
            dir_list = pwd.split('/') + [path]
        else:
            dir_list = [path]
        pwd = '/'.join(dir_list)
    sub_filesystem = reduce(operator.getitem, dir_list, filesystem)
    return sub_filesystem, pwd

with open("day7_input.txt") as file:
    pwd = ''
    filesystem = {}
    sub_filesystem = filesystem
    
    for line in file:
        if "$ cd " in line:
            sub_filesystem, pwd = cd(filesystem, pwd, line.strip().split()[-1])
        elif "$ ls" in line:
            continue
        elif "dir " in line:
            sub_filesystem[line.strip().split()[-1]] = {}
        else:
            size, filename = line.strip().split()
            sub_filesystem[filename] = int(size)


# calculate file sizes

sizes = {}

def calculate_dir_size(filesystem, sizes, pwd):
    dir_size = 0
    for key, value in filesystem.items():
        if isinstance(value, int):
            dir_size += value
        else:
            subdir_path = pwd + f'/{key}'
            subdir_size = calculate_dir_size(value, sizes, subdir_path)
            sizes[subdir_path] = subdir_size
            dir_size += subdir_size
    return dir_size
            
sizes['/'] = calculate_dir_size(filesystem, sizes, '')

answer = 0
for path, size in sizes.items():
    if size <= 100000:
        answer += size

print("part 1: ", answer)

#------

total_usage = sizes['/']
to_free = total_usage - 40000000
dirs_with_enough = [value for value in sizes.values() if value > to_free]
print('Part 2: ', min(dirs_with_enough))
