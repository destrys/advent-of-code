from functools import reduce
import operator


# build a filesystem as nested dictionaries

def cd(filesystem, pwd, path):
    if path == '/':
        return filesystem, ()
    if path == '..':
        pwd = pwd[:-1]
        if not pwd:
            return filesystem, ()
    else:
        pwd += tuple([path])
    sub_filesystem = reduce(operator.getitem, pwd, filesystem)
    return sub_filesystem, pwd

with open("day7_input.txt") as file:
    pwd = ()
    filesystem = {}
    sub_filesystem = filesystem
    
    for line in file:
        if "$ cd " in line:
            sub_filesystem, pwd = cd(filesystem, pwd, line.strip().split()[-1])
        elif "$ ls" in line:
            continue
        elif "dir " in line:
            sub_filesystem[line.strip().split()[-1]] = {}
        else:
            size, filename = line.strip().split()
            sub_filesystem[filename] = int(size)

# calculate directory sizes

def calculate_dir_size(filesystem, dir_sizes, pwd):
    curdir_size = 0
    for key, value in filesystem.items():
        if isinstance(value, int):
            curdir_size += value
        else: # value is a directory (dict)
            subdir_path = pwd + f'/{key}'
            subdir_size = calculate_dir_size(value, dir_sizes, subdir_path)
            dir_sizes[subdir_path] = subdir_size
            curdir_size += subdir_size
    return curdir_size

dir_sizes = {}
dir_sizes['/'] = calculate_dir_size(filesystem, dir_sizes, '')

answer = 0
for path, size in dir_sizes.items():
    if size <= 100000:
        answer += size

print("part 1: ", answer)

#------

total_usage = dir_sizes['/']
to_free = total_usage - 40000000
dirs_with_enough = [value for value in dir_sizes.values() if value > to_free]
print('Part 2: ', min(dirs_with_enough))
