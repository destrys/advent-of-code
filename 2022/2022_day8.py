import itertools

visible = set()

## DOUBLE COUNTING TREES

with open("day8_input.txt") as file:
    rows = [line.strip() for line in file]

def visible_from_left(rows):
    viz = set()
    for row_i, row in enumerate(rows):
        shortest = '-1'
        for col_i, char in enumerate(row):
            if char > shortest:
                viz.add((row_i, col_i))
                shortest = char
                # if char == '9':
                #     break
    return viz

def visible_from_right(rows):
    viz = set()
    for row_i, row in enumerate(rows):
        shortest = '-1'
        for col_i, char in reversed(list(enumerate(row))):
            if char > shortest:
                viz.add((row_i, col_i))
                shortest = char
                # if char == '9':
                #     break
    return viz

def visible_from_top(rows):
    viz = set()
    for col_i in range(len(rows[0])):
        shortest = '-1'
        for rows_i in range(len(rows)):
            if rows[rows_i][col_i] > shortest:
                viz.add((rows_i, col_i))
                shortest = rows[rows_i][col_i]
                # if char == '9':
                #     break
    return viz

def visible_from_bottom(rows):
    viz = set()
    for col_i in range(len(rows[0])):
        shortest = '-1'
        for rows_i in reversed(range(len(rows))):
            if rows[rows_i][col_i] > shortest:
                viz.add((rows_i, col_i))
                shortest = rows[rows_i][col_i]
                # if char == '9':
                #     break
    return viz


visible = visible.union(visible_from_left(rows))
visible = visible.union(visible_from_right(rows))
visible = visible.union(visible_from_top(rows))
visible = visible.union(visible_from_bottom(rows))
print("Part 1: ",len(visible))


with open("day8_input.txt") as file:
    rows = [line.strip() for line in file]

num_rows = len(rows)
num_col = len(rows[0])

best_vision_score = 0
best_list = []
for row_i, col_i in itertools.product(range(num_rows), range(num_col)):
    tree_val = rows[row_i][col_i]
    tree_col = col_i
    tree_row = row_i
    left = 0
    col_i = tree_col - 1
    while col_i >= 0:
        left += 1
        if rows[tree_row][col_i] < tree_val:
            col_i -= 1
        else:
            break
    right = 0
    col_i = tree_col + 1
    while col_i < num_col:
        right += 1        
        if rows[tree_row][col_i] < tree_val:
            col_i += 1
        else:            
            break
    up = 0
    row_i = tree_row - 1
    while row_i >= 0:
        up += 1
        if rows[row_i][tree_col] < tree_val:
            row_i -= 1
        else:
            break
    down = 0
    row_i = tree_row + 1
    while row_i < num_rows:
        down += 1
        if rows[row_i][tree_col] < tree_val:
            row_i += 1
        else:
            break
    scenic_score = left * right * up * down
    if scenic_score > best_vision_score:
        best_vision_score = scenic_score
        best_list = [left, right, up, down]
        best_position = [tree_row, tree_col]

print("Part 2: ", best_vision_score)
                                  
