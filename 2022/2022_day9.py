def move_head(position, direction):
    match direction:
        case 'U':
            return [position[0], position[1] + 1]
        case 'D':
            return [position[0], position[1] - 1]
        case 'L':
            return [position[0] - 1, position[1]]
        case 'R':
            return [position[0] + 1, position[1]]
        case other:
            raise ValueError(f'Direction {direction} unknown')

def move_tail(h_pos, t_pos):
    dist = [h_pos[0] - t_pos[0], h_pos[1] - t_pos[1]]
    if 2 in dist or -2 in dist:
        main_axis = int(abs(dist[1]) == 2)
        other_axis = abs(main_axis - 1)
        t_pos[main_axis] += dist[main_axis]//2
        if dist[other_axis]:
            t_pos[other_axis] += (dist[other_axis] // abs(dist[other_axis]))
    return t_pos

with open("day9_input.txt") as file:
    rope = [[100,100] for _ in range(10)]
    tail_record = set()
    for line in file:
        direction, steps = line.strip().split()
        for _ in range(int(steps)):
            rope[0] = move_head(rope[0], direction)
            rope[1:] = [move_tail(p1, p2) for p1, p2 in zip(rope, rope[1:])]
            tail_record.add(tuple(rope[-1]))

print("Tail Positions: ",len(tail_record))
